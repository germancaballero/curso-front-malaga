/* Esto se ejecuta nada más cargar la página */
var textoClaseVerde = "fondo-verde";
var etiquetaParrafoFinal;
/* Sobre la variable preexitente en JS, ejectuamos el método (acción, función) getElementById, indicando que es un método con los paréntesis, y entre los paréntesis le pasamos los datos (argumentos, parámetros, información) necesaria para que el método funcione */
etiquetaParrafoFinal = document.getElementById("parrafo-final");
// Esto nos devuelve la etiqueta HTML del párrafo, y la guardamos otra una variable llamada etiquetaParrafoFinal

// Ahora, sobre el párrafo, vamos a cambiar su contenido con la propiedad innerHTML, que es una especie de sub-variable, variable miembro, una propiedad de nuestro párrafo, que representa el contido HTML
etiquetaParrafoFinal.innerHTML = "Cambiado por JS";
var contaorDeClicks = 0;

etiquetaParrafoFinal.onclick = function() {
    contaorDeClicks = contaorDeClicks + 1;
    etiquetaParrafoFinal.className = "fondo-verde";
    nombreUsuario = document.getElementById("campo-nombre").value
    mostrarNombreYClicks();
}
var nombreUsuario;

function mostrarNombreYClicks() {
    sumar(contaorDeClicks, 5);
    alert("Nombre usuario: " + nombreUsuario +
        " clicks: " + contaorDeClicks);
}

function sumar(valor1, valor2) {
    alert(valor1 + valor2);
}

sumar(10, 20);