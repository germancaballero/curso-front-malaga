<?php
$mysqli_link = mysqli_connect("localhost", "usuweb", "usuweb1", "bd_juegoesp");

if (mysqli_connect_errno()) 
{
    printf("MySQL connection failed with the error: %s", mysqli_connect_error());
    exit;
} 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Preguntas castellano</title>
</head>
<body>
<h1>Preguntas castellano</h1>
    <audio src="./cosas/mira_nino.mp3" preload="none" controls></audio>
<form>
    <?php     
    $select_query = "SELECT * FROM preguntas";
    $result = mysqli_query($mysqli_link, $select_query);
    $numero_preg = 0;
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        echo "<h2>" . $row['pregunta'] . "</h2>";
        echo '<input type="radio" name="respuesta'.$numero_preg.'" value="1">Sí<br>';
        echo '<input type="radio" name="respuesta'.$numero_preg.'" value="0">No<br>';

        $respuesta_usuario = $_GET['respuesta' . $numero_preg];
        if ($respuesta_usuario == $row['respuesta']) {
            echo "¡¡Muy bien, acertaste!";
        } else {
            echo "¡¡Casi, sigue intentandolo!!";
        }
        
        $numero_preg = $numero_preg + 1;
    }

    // close the db connection
    mysqli_close($mysqli_link);
    ?>

    <input type="submit" value="Enviar">
</form>
</body>
</html>